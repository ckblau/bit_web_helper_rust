mod xencode;
mod util;
mod req;
mod helper;

use clap::{Parser, Subcommand};
use std::process::ExitCode;

#[derive(Parser)]
#[command(about = "Simple tool for automated school web logging-in", long_about = None)]
#[command(author, version)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Check whether you have logged in, return 0 if logged in, 1 if not.
    Check {
    },
    /// Log in, return 0 if succeeded, 1 if not.
    #[command(arg_required_else_help = true)]
    Login {
        /// Usually 1 or 8, you can check this out by directly visiting
        /// http://10.0.0.55 and looking for "ac_id" in the link after redirection.
        #[arg(required = true)]
        ac_id: String,
        /// Your username.
        #[arg(required = true)]
        username: String,
        /// Your password.
        #[arg(required = true)]
        password: String
    },
    /// Log out, return 0 if succeeded, 1 if not.
    #[command(arg_required_else_help = true)]
    Logout {
        /// Your username.
        #[arg(required = true)]
        username: String
    }
}

fn main() -> ExitCode {
    let cli = Cli::parse();
    match cli.command {
        Commands::Check { } => {
            return ExitCode::from(helper::check());
        },
        Commands::Login { ac_id, username, password } => {
            return ExitCode::from(helper::login(&ac_id, &username, &password));
        },
        Commands::Logout { username } => {
            return ExitCode::from(helper::logout(&username));
        }
    }
}